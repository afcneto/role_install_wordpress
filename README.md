# Estudando Molecule
## Criando Ambiente virtual python3 isolado.
```
python3 -m venv nome-do-ambiente
```
> python3 -m venv venv

## Ativando ambiente virtual
```
source venv/bin/activate
```
> source venv/bin/activate

## Instalando dependencias
```
python3 -m pip install "molecule[ansible, docker, lint]"
```

## Criar uma Role para instalar o wordpress

1. Update APT 
--- Descrever Passo a passo.
2. APT install apache
3. APT install Mysql
4. APT install PHP
5. Download wordpress


